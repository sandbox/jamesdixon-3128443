<?php
/**
 * @file
 * Contains \Drupal\migrate_urllist\Plugin\migrate\source\urllist.
 */

namespace Drupal\migrate_urllist\Plugin\migrate\source;

use Drupal\migrate_plus\Plugin\migrate\source\Url;
use Drupal\migrate\Plugin\MigrationInterface;

/**
 * Creates a source from a list of files in a directory.
 *
 * @MigrateSource(
 *   id = "urllist"
 * )
 *
 * To migrate a list of xml files from a directory use the following:
 *
 * @code
 *   source:
 *     plugin: 'urllist'
 *     data_fetcher_plugin: http
 *     data_parser_plugin: xml
 *     list_directory: private://your-folder
 *     allowed_extensions: xml
 *     item_selector: /your/selector
 * @endcode
 */
class UrlList extends Url {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration) {
    $list_directory = $configuration['list_directory'];
    $allowed_extensions = $configuration['allowed_extensions'];
    $urls = $this->ListFiles($list_directory, $allowed_extensions);
    $configuration['urls'] = $urls;

    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);

    $this->sourceUrls = $configuration['urls'];
  }

  /**
   * Recursively iterates through the list directory to find all files there
   * and return the files as urls. Excludes files not in allowed extensions.
   *
   * @param string $dir
   *   A stream wrapper URI that is a directory.
   *
   * @param string $allowed_extensions
   *   The allowed file type extensions separated by spaces. (ie: xml html)
   *
   * @return string[]
   *   An array of stream wrapper URIs pointing to files.
   */
  protected function listFiles($dir, $allowed_extensions) {
    $flags =
      \FilesystemIterator::KEY_AS_PATHNAME |
      \FilesystemIterator::CURRENT_AS_FILEINFO |
      \FilesystemIterator::SKIP_DOTS;

    if (!is_dir($dir) || !is_readable($dir)) {
      throw new \RuntimeException($this->t('%source is not a readable directory or file. Check your list_directory configuration.', ['%source' => $dir]));
    }

    // :TODO: Add configuration option for recursive rather than assuming.
    //if ($this->configuration['recursive_scan']) {
      $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dir, $flags));
    /*}
    else {
      $iterator = new \FilesystemIterator($dir, $flags);
    }*/

    // Find each file from list_directory with a valid extension. Ignore files
    // which do not have valid extensions.
    $files = [];
    foreach ($iterator as $path => $file) {
      if ($file->isFile() && $file->isReadable() && UrlList::validateExtension($path, $allowed_extensions)) {
        $files[] = $path;
      }
    }

    return $files;
  }

  /**
   * Checks that the filename ends with an allowed extension.
   *
   * @param string $filename
   *   A file path.
   * @param string $extensions
   *   A string with a space separated list of allowed extensions.
   *
   * @return bool
   *   Returns true if the file is valid, false if not.
   */
  protected static function validateExtension($filename, $extensions) {
    $regex = '/\.(' . preg_replace('/ +/', '|', preg_quote($extensions)) . ')$/i';

    return (bool) preg_match($regex, $filename);
  }

}
